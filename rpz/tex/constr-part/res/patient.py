class Patient(models.Model):

    patient_id = models.AutoField(_("patient id"), primary_key=True)
    user = models.OneToOneField(User)
    second_name = models.CharField(_("second name"), max_length=100)
    first_name = models.CharField(_("first name"), max_length=100)
    patronymic = models.CharField(_("patronymic"), max_length=100)
    gender = models.CharField(_("gender"), max_length=1)
    series_of_passport = models.CharField(_("series of passport"), max_length=4)
    passport_number = models.CharField(_("passport number"), max_length=6)
    assurance_number = models.CharField(_("assurance number"), max_length=16)
    date_of_birth = models.DateField(_("date of birth"))
    phone_number = models.CharField(_("phone number"), max_length=11)

