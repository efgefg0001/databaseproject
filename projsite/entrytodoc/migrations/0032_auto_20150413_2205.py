# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import execute_from_command_line

def load_data_from_fixture(apps, schema_editor):
    execute_from_command_line(["manage.py", "loaddata", "patient"])

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0031_auto_20150413_2204'),
    ]

    operations = [
        migrations.RunPython(load_data_from_fixture)
    ]
