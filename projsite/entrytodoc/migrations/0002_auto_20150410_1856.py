# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import execute_from_command_line


def load_polyclinics_from_fixture(apps, schema_editor):
    execute_from_command_line(["manage.py", "loaddata", "polyclinic"])

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_polyclinics_from_fixture),
    ]
