# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from datetime import datetime, date, timedelta
import re

from django.db import models, migrations
from django.db.models import Q
from entrytodoc.models import *

def create_letters(apps, schema_editor):
    beg_date = date(2015, 5, 6)
    end_date = date(2015, 5, 8)
    days_count = (end_date - beg_date).days + 1
    let_per_day = 16
    let_per_day_min_1 = let_per_day - 1
    for i in range(days_count):
        cur_date = beg_date + timedelta(days=i)
        docs = Doctor.objects.all()
        for doc in docs:
            is_inactive = False
            inact_days = InactiveDays.objects.filter(Q(start_date__lte=cur_date) & Q(end_date__gte=cur_date))
            for day in inact_days:
                doc_match = re.match(day.doctors_regex, str(doc.pk))
                if doc_match is not None:
                    is_inactive = True
                    break
            if not is_inactive:
                weekday = cur_date.weekday()
                schedule_row = Schedule.objects.filter(Q(day_of_week=weekday) & Q(doctor=doc.pk))[0]
                time_beg = datetime.combine(cur_date, schedule_row.start_time)
                time_end = datetime.combine(cur_date, schedule_row.end_time)
                delta1 = time_end - time_beg
                delta = (time_end - time_beg).seconds/let_per_day_min_1
                step = timedelta(seconds=delta)
                start_t = schedule_row.start_time
                for j in range(let_per_day):
                    cur_time = time_beg + timedelta(seconds=j*step.seconds)
                    letter = Letter(
#                        patient = models.ForeignKey(Patient, null=True, blank=True)
                        doctor = doc,
                        time = cur_time.time(),
                        date = cur_date,
                    )
                    letter.save()


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0040_auto_20150505_1236'),
    ]

    operations = [
        migrations.RunPython(create_letters)
    ]
