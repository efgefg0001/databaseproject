# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import entrytodoc.cusfields


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0022_auto_20150412_2144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='doctor_id',
            field=models.AutoField(verbose_name='doctor id', primary_key=True, serialize=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doctor',
            name='first_name',
            field=models.CharField(verbose_name='first name', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doctor',
            name='patronymic',
            field=models.CharField(verbose_name='patronymic', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doctor',
            name='phone_number',
            field=models.CharField(verbose_name='phone number', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doctor',
            name='second_name',
            field=models.CharField(verbose_name='second name', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doctor',
            name='speciality',
            field=models.CharField(verbose_name='speciality', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='letter',
            name='date',
            field=models.DateField(verbose_name='date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='letter',
            name='letter_id',
            field=models.AutoField(verbose_name='letter id', primary_key=True, serialize=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='letter',
            name='time',
            field=models.TimeField(verbose_name='time'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='patient',
            name='patient_id',
            field=models.AutoField(verbose_name='patient id', primary_key=True, serialize=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='building',
            field=models.CharField(verbose_name='building', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='location',
            field=models.CharField(verbose_name='location', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='polyclinic_id',
            field=models.AutoField(verbose_name='polyclinic id', primary_key=True, serialize=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='polyclinic_name',
            field=models.CharField(verbose_name='polyclinic name', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='region',
            field=models.CharField(verbose_name='region', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='polyclinic',
            name='street',
            field=models.CharField(verbose_name='street', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='cabinet_number',
            field=models.IntegerField(verbose_name='cabinet number'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='day_of_week',
            field=entrytodoc.cusfields.DayOfTheWeekField(choices=[('0', 'Monday'), ('1', 'Tuesday'), ('2', 'Wednesday'), ('3', 'Thursday'), ('4', 'Friday'), ('5', 'Saturday'), ('6', 'Sunday')], verbose_name='day of week', max_length=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='end_time',
            field=models.TimeField(verbose_name='end time'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='schedule_row_id',
            field=models.AutoField(verbose_name='schedule row id', primary_key=True, serialize=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='start_time',
            field=models.TimeField(verbose_name='start time'),
            preserve_default=True,
        ),
    ]
