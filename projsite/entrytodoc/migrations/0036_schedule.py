# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import entrytodoc.cusfields


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0035_auto_20150427_2037'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('schedule_row_id', models.AutoField(verbose_name='schedule row id', primary_key=True, serialize=False)),
                ('day_of_week', entrytodoc.cusfields.DayOfTheWeekField(verbose_name='day of week', choices=[('0', 'Monday'), ('1', 'Tuesday'), ('2', 'Wednesday'), ('3', 'Thursday'), ('4', 'Friday'), ('5', 'Saturday'), ('6', 'Sunday')], max_length=1)),
                ('start_time', models.TimeField(verbose_name='start time')),
                ('end_time', models.TimeField(verbose_name='end time')),
                ('cabinet_number', models.IntegerField(verbose_name='cabinet number')),
                ('doctor', models.ForeignKey(to='entrytodoc.Doctor')),
                ('polyclinic', models.ForeignKey(to='entrytodoc.Polyclinic')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
