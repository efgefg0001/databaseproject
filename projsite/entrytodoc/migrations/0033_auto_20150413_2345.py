# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from ..models import *

def change_passwords(apps, schema_editor):
    count = len(User.objects.all())
    base = "password"
    for i in range(count):
        user = User.objects.get(id=i+1)
        user.set_password(base + str(i+1))
        user.save()

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0032_auto_20150413_2205'),
    ]

    operations = [
    ]
