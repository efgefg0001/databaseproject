# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from entrytodoc.models import *

def add_usernames_and_passwords_for_doctors(apps, schema_editor):
    base_uname = "username"
    base_pas = "password"
    index = 8001
    doctors = Doctor.objects.all()
    for doctor in doctors:
        user = User()
        user.username = base_uname + str(index)
        user.set_password(base_pas + str(index))
        user.save()
        doctor.user = user
        index += 1

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0050_doctor_user'),
    ]

    operations = [
        migrations.RunPython(add_usernames_and_passwords_for_doctors)
    ]
