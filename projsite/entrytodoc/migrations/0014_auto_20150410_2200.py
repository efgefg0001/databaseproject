# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0013_auto_20150410_2158'),
    ]

    operations = [
        migrations.CreateModel(
            name='Letter',
            fields=[
                ('letter_id', models.AutoField(primary_key=True, serialize=False)),
                ('time', models.TimeField()),
                ('date', models.DateField()),
                ('doctor_id', models.ForeignKey(to='entrytodoc.Doctor')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('patient_id', models.AutoField(primary_key=True, serialize=False)),
                ('second_name', models.CharField(max_length=100, verbose_name='second name')),
                ('first_name', models.CharField(max_length=100, verbose_name='first name')),
                ('patronymic', models.CharField(max_length=100, verbose_name='patronymic')),
                ('gender', models.CharField(max_length=1, verbose_name='gender')),
                ('series_of_passport', models.CharField(max_length=4, verbose_name='series of passport')),
                ('passport_number', models.CharField(max_length=6, verbose_name='passport number')),
                ('assurance_number', models.CharField(max_length=16, verbose_name='assurance number')),
                ('date_of_birth', models.DateField(verbose_name='date of birth')),
                ('phone_number', models.CharField(max_length=11, verbose_name='phone number')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='letter',
            name='patient_id',
            field=models.ForeignKey(to='entrytodoc.Patient'),
            preserve_default=True,
        ),
    ]
