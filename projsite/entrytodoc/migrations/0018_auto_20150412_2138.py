# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0017_auto_20150412_1900'),
    ]

    operations = [
        migrations.DeleteModel("Patient"),
    ]
