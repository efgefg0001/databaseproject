# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('doctor_id', models.AutoField(primary_key=True, serialize=False)),
                ('second_name', models.CharField(max_length=100)),
                ('first_name', models.CharField(max_length=100)),
                ('patronymic', models.CharField(max_length=100)),
                ('speciality', models.CharField(max_length=100)),
                ('phone_number', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Letter',
            fields=[
                ('letter_id', models.AutoField(primary_key=True, serialize=False)),
                ('time', models.TimeField()),
                ('date', models.DateField()),
                ('doctor_id', models.ForeignKey(to='entrytodoc.Doctor')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('patient_id', models.AutoField(primary_key=True, serialize=False)),
                ('second_name', models.CharField(verbose_name='second name', max_length=100)),
                ('first_name', models.CharField(verbose_name='first name', max_length=100)),
                ('patronymic', models.CharField(verbose_name='patronymic', max_length=100)),
                ('gender', models.CharField(verbose_name='gender', max_length=1)),
                ('series_of_passport', models.IntegerField(verbose_name='series of passport', max_length=4)),
                ('passport_number', models.CharField(verbose_name='passport number', max_length=6)),
                ('assurance_number', models.CharField(verbose_name='assurance number', max_length=16)),
                ('date_of_birth', models.DateField(verbose_name='date of birth')),
                ('phone_number', models.CharField(verbose_name='phone number', max_length=11)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Polyclinic',
            fields=[
                ('polyclinic_id', models.AutoField(primary_key=True, serialize=False)),
                ('region', models.CharField(max_length=100)),
                ('location', models.CharField(max_length=100)),
                ('street', models.CharField(max_length=100)),
                ('building', models.CharField(max_length=100)),
                ('polyclinic_name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('schedule_row_id', models.AutoField(primary_key=True, serialize=False)),
                ('day_of_week', models.IntegerField()),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('cabinet_number', models.IntegerField()),
                ('doctor_id', models.ForeignKey(to='entrytodoc.Doctor')),
                ('polyclinic_id', models.ForeignKey(to='entrytodoc.Polyclinic')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='letter',
            name='patient_id',
            field=models.ForeignKey(to='entrytodoc.Patient'),
            preserve_default=True,
        ),
    ]
