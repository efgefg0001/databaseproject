# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from entrytodoc.models import *

def add_users_for_doctors(apps, schema_editor):
    index = 1
    users = User.objects.filter(pk__gt=8000)
    for user in users:
        doctor = Doctor.objects.get(pk=index)
        doctor.user = user
        doctor.save()
        index += 1

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0051_auto_20150507_1419'),
    ]

    operations = [
        migrations.RunPython(add_users_for_doctors)
    ]
