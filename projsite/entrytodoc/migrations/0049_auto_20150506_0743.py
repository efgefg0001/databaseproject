# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from entrytodoc.models import *

def change_doctors_regex(apps, schema_editor):
    count = len(InactiveDays.objects.all())
    regex = r"^([1-9]|[1-9]\d|[1-5]\d{2}|600)$"
    for i in range(count):
        inact_days = InactiveDays.objects.get(pk=i+1)
        inact_days.doctors_regex = regex
        inact_days.save()

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0048_auto_20150505_1931'),
    ]

    operations = [
        migrations.RunPython(change_doctors_regex)
    ]
