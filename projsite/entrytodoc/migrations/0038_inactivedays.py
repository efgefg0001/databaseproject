# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0037_auto_20150427_2041'),
    ]

    operations = [
        migrations.CreateModel(
            name='InactiveDays',
            fields=[
                ('inactive_days_id', models.AutoField(primary_key=True, serialize=False, verbose_name='inactive day id')),
                ('start_date', models.DateField(verbose_name='start date')),
                ('end_date', models.DateField(verbose_name='end date')),
                ('doctors_regex', models.CharField(max_length=100, verbose_name='doctors regex')),
                ('description', models.CharField(max_length=100, verbose_name='description')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
