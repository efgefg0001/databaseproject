# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import execute_from_command_line


def load_data_from_fixture(apps, schema_editor):
    execute_from_command_line(["manage.py", "loaddata", "polyclinic"])
    execute_from_command_line(["manage.py", "loaddata", "patient"])
    execute_from_command_line(["manage.py", "loaddata", "doctor"])
    execute_from_command_line(["manage.py", "loaddata", "schedule"])

class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0021_auto_20150412_2143'),
    ]

    operations = [
        migrations.RunPython(load_data_from_fixture),
    ]
