# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0028_auto_20150413_2154'),
    ]

    operations = [
        migrations.DeleteModel("Patient")
    ]
