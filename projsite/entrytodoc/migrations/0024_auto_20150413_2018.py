# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0023_auto_20150412_2204'),
    ]

    operations = [
        migrations.RenameField(
            model_name='letter',
            old_name='doctor_id',
            new_name='doctor',
        ),
        migrations.RenameField(
            model_name='letter',
            old_name='patient_id',
            new_name='patient',
        ),
        migrations.RenameField(
            model_name='schedule',
            old_name='doctor_id',
            new_name='doctor',
        ),
        migrations.RenameField(
            model_name='schedule',
            old_name='polyclinic_id',
            new_name='polyclinic',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='patient',
            field=models.OneToOneField(default=1, to='entrytodoc.Patient'),
            preserve_default=False,
        ),
    ]
