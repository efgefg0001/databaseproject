# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0052_auto_20150507_1446'),
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('log_id', models.AutoField(serialize=False, verbose_name='log id', primary_key=True)),
                ('pat_second_name', models.CharField(max_length=100, verbose_name='second name')),
                ('pat_first_name', models.CharField(max_length=100, verbose_name='first name')),
                ('pat_patronymic', models.CharField(max_length=100, verbose_name='patronymic')),
                ('pat_gender', models.CharField(max_length=1, verbose_name='gender')),
                ('pat_series_of_passport', models.CharField(max_length=4, verbose_name='series of passport')),
                ('pat_passport_number', models.CharField(max_length=6, verbose_name='passport number')),
                ('pat_assurance_number', models.CharField(max_length=16, verbose_name='assurance number')),
                ('pat_date_of_birth', models.DateField(verbose_name='date of birth')),
                ('pat_phone_number', models.CharField(max_length=11, verbose_name='phone number')),
                ('doc_second_name', models.CharField(max_length=100, verbose_name='second name')),
                ('doc_first_name', models.CharField(max_length=100, verbose_name='first name')),
                ('doc_patronymic', models.CharField(max_length=100, verbose_name='patronymic')),
                ('doc_speciality', models.CharField(max_length=100, verbose_name='speciality')),
                ('doc_phone_number', models.CharField(max_length=100, verbose_name='phone number')),
                ('letter_date', models.DateField()),
                ('letter_time', models.TimeField()),
                ('doc_took', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
