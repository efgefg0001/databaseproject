# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from entrytodoc.models import *


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0043_auto_20150505_1308'),
    ]

    operations = [
        migrations.DeleteModel("Letter")
    ]
