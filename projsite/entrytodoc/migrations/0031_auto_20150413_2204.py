# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('entrytodoc', '0030_auto_20150413_2156'),
    ]

    operations = [
        migrations.CreateModel(
            name='Letter',
            fields=[
                ('letter_id', models.AutoField(serialize=False, verbose_name='letter id', primary_key=True)),
                ('time', models.TimeField(verbose_name='time')),
                ('date', models.DateField(verbose_name='date')),
                ('doctor', models.ForeignKey(to='entrytodoc.Doctor')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('patient_id', models.AutoField(serialize=False, verbose_name='patient id', primary_key=True)),
                ('second_name', models.CharField(max_length=100, verbose_name='second name')),
                ('first_name', models.CharField(max_length=100, verbose_name='first name')),
                ('patronymic', models.CharField(max_length=100, verbose_name='patronymic')),
                ('gender', models.CharField(max_length=1, verbose_name='gender')),
                ('series_of_passport', models.CharField(max_length=4, verbose_name='series of passport')),
                ('passport_number', models.CharField(max_length=6, verbose_name='passport number')),
                ('assurance_number', models.CharField(max_length=16, verbose_name='assurance number')),
                ('date_of_birth', models.DateField(verbose_name='date of birth')),
                ('phone_number', models.CharField(max_length=11, verbose_name='phone number')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='letter',
            name='patient',
            field=models.ForeignKey(to='entrytodoc.Patient'),
            preserve_default=True,
        ),
    ]
