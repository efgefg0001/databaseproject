# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0016_auto_20150410_2215'),
    ]

    operations = [
        migrations.DeleteModel("Letter"),
    ]
