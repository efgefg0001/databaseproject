# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0053_log'),
    ]

    operations = [
        migrations.AddField(
            model_name='letter',
            name='log',
            field=models.ForeignKey(null=True, to='entrytodoc.Log', blank=True),
            preserve_default=True,
        ),
    ]
