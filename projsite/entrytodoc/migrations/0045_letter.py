# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0044_auto_20150505_1856'),
    ]

    operations = [
        migrations.CreateModel(
            name='Letter',
            fields=[
                ('letter_id', models.AutoField(serialize=False, verbose_name='letter id', primary_key=True)),
                ('time', models.TimeField(verbose_name='time')),
                ('date', models.DateField(verbose_name='date')),
                ('doctor', models.ForeignKey(to='entrytodoc.Doctor')),
                ('patient', models.ForeignKey(null=True, to='entrytodoc.Patient', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
