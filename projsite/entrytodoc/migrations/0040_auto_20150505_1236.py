# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entrytodoc', '0039_auto_20150502_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='letter',
            name='patient',
            field=models.ForeignKey(blank=True, null=True, to='entrytodoc.Patient'),
            preserve_default=True,
        ),
    ]
