#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from datetime import *
from collections import namedtuple, defaultdict

from django.utils import timezone
from django.db.models import Q

from entrytodoc.models import *


def get_today():
    """
    Получить сегодняшнюю дату.
    """
    return datetime.today()


def get_now():
    """
    Получить сегодняшнюю дату и текущее время.
    """
    return datetime.now()


def get_doctors(polyclinic):
    """
    Получить всех докторов в поликлинике.
    """
    doc_ids = Schedule.objects.filter(polyclinic=polyclinic).order_by("doctor").values("doctor").distinct()
    doc_list = []
    i = 1
    for id in doc_ids:
        doc_list.append(
            (id["doctor"], Doctor.objects.get(pk=id["doctor"]))
        )
        i += 1
    return doc_list


WEEKDAYS = [
    "Mo",
    "Tu",
    "We",
    "Th",
    "Fr",
    "Sa",
    "Su"
]
WEEKDAYS_RUS = [
    "Пн",
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб",
    "Вс"
]


Day = namedtuple("Day", "id date is_inactive")
Calendar = namedtuple("Calendar", "weekdays dates")

def get_schedule(doc, duration):
    """
    Получить расписание для доктора doc за
    промежуток дат duration.
    """
    calendar = Calendar(
        weekdays=get_weekdays(),
        dates=get_dates(doc, duration)
    )
    return calendar


def get_weekdays():
    """
    Получить дни недели, начиная с сегодняшнего дня.
    """
    today = get_today()
    weekdays = []
    beg_weekday = today.weekday()
    week_len = len(WEEKDAYS_RUS)
    for i in range(week_len):
        cur_weekday = (beg_weekday + i) % week_len
        weekdays.append(WEEKDAYS_RUS[cur_weekday])
    return weekdays


def get_dates(doc, duration):
    """
    Получить даты рабочих дней для доктора doc
    за промежуток дат duration.
    """
    today = get_today()
    result_days = [[]]
    i = 0
    index = 0
    week_len = len(WEEKDAYS)
    for delta in range(duration):
        if delta % week_len == 0:
            result_days.append([])
            index += 1
        cur_day = today + timedelta(days=delta)
        is_inactive_day = False
        inact_days = InactiveDays.objects.filter(Q(start_date__lte=cur_day) & Q(end_date__gte=cur_day))
        for inact_day in inact_days:
            doc_match = re.match(inact_day.doctors_regex, str(doc))
            if doc_match is not None:
                is_inactive_day = True
                break
        i += 1
        result_days[index].append(Day(i, cur_day, is_inactive_day))
    return result_days


def get_patient_id(username):
    """
    Получить идентификатор пациента по имени
    пользователя.
    """
    patient_id = 0
    users = User.objects.filter(username=username)
    if len(users) > 0:
        user_id = users[0].pk
        patients = Patient.objects.filter(user=user_id)
        if len(patients) > 0:
            patient_id = patients[0].pk
    return patient_id


def get_doctor_id(username):
    """
    Получить идентификатор доктора по имени
    пользователя.
    """
    doctor_id = 0
    users = User.objects.filter(username=username)
    if len(users) > 0:
        user_id = users[0].pk
        doctors = Doctor.objects.filter(user=user_id)
        if len(doctors) > 0:
            doctor_id = doctors[0].pk
    return doctor_id


LetterTime = namedtuple("LetterTime", "letter_id time can_take")

def get_letter_times(letter_buf):
    """
    Получить времена талонов для заданного доктора
    и заданной даты.
    """
    letters = Letter.objects.filter(Q(date=letter_buf.date) & Q(doctor=letter_buf.doctor)).order_by("pk")
    letter_times = [[]]
    letters_in_row = 6
    count, index = 0, 0
    check_time = False
    now = get_now()
    print("now = {}".format(now))
    if letter_buf.date == now.date():
        check_time = True
    for letter in letters:
        can_take = True
        count += 1
        if letter.patient is not None:
            can_take = False
        if check_time:
            if letter.time < now.time():
                can_take = False
        letter_times[index].append(LetterTime(letter.pk, letter.time, can_take))
        if count % letters_in_row == 0:
            letter_times.append([])
            index += 1
    return letter_times


LetterDoctorView = namedtuple("LetterDoctorView", "letter_id doc_name doc_spec date time")

def get_your_active_letters(patient_id):
    """
    Получить активные талоны для пациента
    с идентификатором patient_id.
    """
    patient_letters = Letter.objects.filter(patient=patient_id).order_by("date").order_by("time")
    now = get_now()
    active_letters = []
    for patient_letter in patient_letters:
        letter_datetime = datetime.combine(patient_letter.date, patient_letter.time)
        if letter_datetime >= now:
            doc = patient_letter.doctor
            doc_full_name = "{} {} {}".format(doc.second_name, doc.first_name, doc.patronymic)
            active_letters.append(
                LetterDoctorView(patient_letter.pk, doc_full_name, doc.speciality, patient_letter.date, patient_letter.time)
            )
    return active_letters


LetterPatientView = namedtuple("LetterPatientView", "letter_id full_name date time phone_num assurance_num")

def get_my_patients(doctor_id):
    """
    Получить список записанных пациентов для
    доктора с идентификатором doctor_id.
    """
    doctor_letters = Letter.objects.filter(doctor=doctor_id).order_by("date").order_by("time")
    today = get_today().date()
    active_letters = []
    for doctor_letter in doctor_letters:
        letter_date = doctor_letter.date
        if doctor_letter.patient is not None and letter_date >= today:
            patient = doctor_letter.patient
            patient_full_name = "{} {} {}".format(patient.second_name, patient.first_name, patient.patronymic)
            active_letters.append(
                LetterPatientView(
                    letter_id=doctor_letter.pk,
                    full_name=patient_full_name, date=letter_date, time=doctor_letter.time,
                    phone_num=patient.phone_number, assurance_num=patient.assurance_number
                )
            )
    sorted(active_letters, )
    return active_letters




