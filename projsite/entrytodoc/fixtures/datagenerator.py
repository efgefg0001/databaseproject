#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import json
import abc
from datetime import *
from collections import namedtuple
from dateutil.relativedelta import *
import sys

class Generator(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_item(self):
        raise NotImplementedError()

    def get_items(self, count):
        items = []
        for i in range(count):
            items.append(self.get_item())
        return items


class PersonGenerator(Generator):

    MIN_PH_NUM = 79000000000
    MAX_PH_NUM = 79900000000

    def __init__(self, f_names, s_names, patronymics):
        self.__ph_nums = set()
        self.__f_names = f_names
        self.__s_names = s_names
        self.__patronymics = patronymics
        self.__genders = ["М", "Ж"]
        self.Person = namedtuple("Person", "fname sname patronymic gender phone_num")

    def get_unique_num(self, low, high, num_set):
        number = random.randint(low, high)
        while number in num_set:
            number = random.randint(low, high)
        num_set.add(number)
        return number

    def get_name(self, gender):
        s_name = random.choice(self.__s_names[gender])
        f_name = random.choice(self.__f_names[gender])
        patr = random.choice(self.__patronymics[gender])
        return s_name, f_name, patr

    def get_item(self):
        phone_num = self.get_unique_num(
            self.MIN_PH_NUM, self.MAX_PH_NUM, self.__ph_nums
        )
        gender = random.choice(self.__genders)
        s_name, f_name, patr = self.get_name(gender)
        return self.Person(f_name, s_name, patr, gender, phone_num)


class UserGenerator(Generator):

    def __init__(self):
        self.__base_uname = "username"
        self.__base_pas = "password"
        self.__pk = 0

    def get_item(self):
        self.__pk += 1
        username = self.__base_uname + str(self.__pk)
        password = self.__base_pas + str(self.__pk)
        user = {
            "model": "auth.user",
            "pk": self.__pk,
            "fields": {
                "username": username,
                "password": password
            }
        }
        return user


class PatientGenerator(PersonGenerator):

    MIN_ASSUR_NUM = 10 ** 15
    MAX_ASSUR_NUM = 9 * 10 ** 15
    MIN_PAS_NUM = 10 ** 9
    MAX_PAS_NUM = 9 * 10 ** 9
    START_DATE = date(1900, 1, 1)
    END_DATE = date(2015, 1, 1)
    DATE_FORMAT = "%Y-%m-%d"

    def __init__(self, f_names, s_names, patronymics):
        super().__init__(f_names, s_names, patronymics)
        self.__pas_nums = set()
        self.__assur_nums = set()
        self.__ten6 = 10 ** 6
        self.__delta = self.END_DATE - self.START_DATE
        self.__pk = 0

    def __get_date(self):
        rand_day = random.randint(0, self.__delta.days)
        date = self.START_DATE + timedelta(days=rand_day)
        return date.strftime(self.DATE_FORMAT)

    def get_item(self):
        person = super().get_item()
        assur_num = self.get_unique_num(
            self.MIN_ASSUR_NUM, self.MAX_ASSUR_NUM, self.__assur_nums
        )
        pas_num = self.get_unique_num(
            self.MIN_PAS_NUM, self.MAX_PAS_NUM, self.__pas_nums
        )
        series = pas_num // self.__ten6
        pas_num %= self.__ten6
        date_of_birth = self.__get_date()
        self.__pk += 1
        patient = {
            "model": "entrytodoc.Patient",
            "pk": str(self.__pk),
            "fields": {
                "user": self.__pk,
                "second_name": person.sname,
                "first_name": person.fname,
                "patronymic": person.patronymic,
                "gender": person.gender,
                "series_of_passport": str(series),
                "passport_number": str(pas_num),
                "assurance_number": str(assur_num),
                "date_of_birth": date_of_birth,
                "phone_number": str(person.phone_num)
            }
        }
        return patient


class  DoctorGenerator(PersonGenerator):

    def __init__(self, f_names, s_names, patronymics, specs):
        super().__init__(f_names, s_names, patronymics)
        self.__specs = specs
        self.__pk = 0

    def get_item(self):
        person = super().get_item()
        spec = random.choice(self.__specs)
        self.__pk += 1
        doctor = {
            "model": "entrytodoc.Doctor",
            "pk": self.__pk,
            "fields": {
                "second_name": person.sname,
                "first_name": person.fname,
                "patronymic": person.patronymic,
                "speciality": spec,
                "phone_number": str(person.phone_num)
            }
        }
        return doctor


class AddressGenerator(Generator):

    MIN_NUM = 1
    MAX_NUM = 25

    def __init__(self, regions_cities, streets):
        self.__regs_cits = regions_cities
        self.__regions = list(self.__regs_cits.keys())
        self.__streets = streets
        self.__addr_set = set()
        self.Addr = namedtuple("Addr", "region city street building")

    def __get_addr(self):
        region = random.choice(self.__regions)
        city = random.choice(self.__regs_cits[region])
        street = random.choice(self.__streets)
        building = random.randint(self.MIN_NUM, self.MAX_NUM)
        return self.Addr(region, city, street, building)

    def get_item(self):
        addr = self.__get_addr()
        while addr in self.__addr_set:
            addr = self.__get_addr()
        self.__addr_set.add(addr)
        return addr


class PolyclinicGenerator(Generator):

    MIN_NUM = 1
    MAX_NUM = 25

    def __init__(self, regions_cities, streets):
        self.__addr_gen = AddressGenerator(regions_cities, streets)
        self.__regions_cities = regions_cities
        self.__streets = streets
        self.__pk = 0
        self.__cl_gen_name = "Поликлиника №"

    def get_item(self):
        address = self.__addr_gen.get_item()
        self.__pk += 1
        name = self.__cl_gen_name + str(self.__pk)
        polyclinic = {
            "model": "entrytodoc.Polyclinic",
            "pk": self.__pk,
            "fields": {
                "region": address.region,
                "location": address.city,
                "street": address.street,
                "building": address.building,
                "polyclinic_name": name
            }
        }
        return polyclinic


class ScheduleGenerator(Generator):

    MON_NUM = 0
    SUN_NUM = 6
    MIN_CAB = 1
    DATETIME_FORMAT = "%H:%M"

    def __init__(self, num_of_docs, num_of_clinics, docs_per_clinic):
        self.__num_of_docs = num_of_docs
        self.__num_of_clinics = num_of_clinics
        self.MAX_CAB = num_of_docs // num_of_clinics
        self.__docs_per_clinic = docs_per_clinic
        self.__pk = 0
        self.__work_days = 5
        self.__curr_day = -1
        self.__doc_id = 1
        self.__doc_count = 0
        self.__clinic_id = 1
        self.__start_t = datetime(1, 1, 1, hour=8, minute=0)
        self.__cl_end_t = datetime(1, 1, 1, hour=18, minute=0)
        self.__work_hours = timedelta(hours=4)
        self.__th_end_t = self.__cl_end_t -  self.__work_hours
        self.__delta = relativedelta(self.__th_end_t - self.__start_t)

    def __get_doc_and_weekday(self):
        self.__curr_day += 1
        if self.__curr_day >= self.__work_days:
            self.__doc_id += 1
            self.__doc_count += 1
            self.__curr_day = 0
        return self.__doc_id, self.__curr_day

    def __get_clinic(self):
        if self.__doc_count >= self.__docs_per_clinic:
#            self.__doc_id = 1
            self.__doc_count = 0
            self.__clinic_id += 1
        return self.__clinic_id

    def __get_cabinet(self):
        return self.__doc_count + 1

    def  __get_start_end_t(self):
        start_t = datetime(1, 1, 1,
                           random.randint(self.__start_t.hour, self.__th_end_t.hour),
                           0)
        end_t = start_t + self.__work_hours
        return start_t, end_t

    def get_item(self):
        self.__pk += 1
        doc, weekday = self.__get_doc_and_weekday()
        clinic = self.__get_clinic()
        cabinet = self.__get_cabinet()
        start_t, end_t = self.__get_start_end_t()
        schedule = {
            "model": "entrytodoc.Schedule",
            "pk": self.__pk,
            "fields": {
                "doctor": doc,
                "day_of_week": weekday,
                "start_time": start_t.strftime(self.DATETIME_FORMAT),
                "end_time": end_t.strftime(self.DATETIME_FORMAT),
                "polyclinic": clinic,
                "cabinet_number": cabinet
            }
        }
        return schedule


class LetterGenerator(Generator):

    def __init__(self):
        pass

    def get_item(self):
        pass

InactiveDuration = namedtuple("InactiveDuration", "doc_regex, beg end descr")
class InactiveDaysGenerator(Generator):

    DATE_FORMAT = "%Y-%m-%d"
    BEG_DATE = date(2015, 3, 1)
    END_DATE = date(2015, 9, 1)

    def __init__(self):
        self.__doctors_regex = r"^([1-9]|[1-9]\d|[1-2]\d{2}|300)$"
        self.__inactive_days = self.__get_holidays()
        self.__fill_weekends()
        self.__cur_item = 0
        self.__pk = 0


    def __get_holidays(self):
        descr = "Праздники"
        holidays = [
            InactiveDuration(self.__doctors_regex, date(2015, 3, 8), date(2015, 3, 9),descr),
            InactiveDuration(self.__doctors_regex, date(2015, 5, 1), date(2015, 5, 4),descr),
            InactiveDuration(self.__doctors_regex, date(2015, 5, 9), date(2015, 5, 11),descr),
            InactiveDuration(self.__doctors_regex, date(2015, 6, 12), date(2015, 6, 12),descr),
        ]
        return holidays


    def __fill_weekends(self):
        cur_date = self.BEG_DATE
        descr = "Выходные дни"
        while cur_date != self.END_DATE:
            if cur_date.weekday() == 5:
                beg, end = cur_date, cur_date
                next_date = cur_date + timedelta(days=1)
                if next_date < self.END_DATE and next_date.weekday() == 6:
                    end = next_date
                    cur_date = next_date + timedelta(days=1)
                else:
                    cur_date = next_date
                self.__inactive_days.append(
                    InactiveDuration(self.__doctors_regex, beg, end, descr)
                )
            elif cur_date.weekday() == 6:
                beg, end = cur_date, cur_date
                self.__inactive_days.append(
                    InactiveDuration(self.__doctors_regex, beg, end, descr)
                )
                cur_date = cur_date + timedelta(days=1)
            else:
                cur_date = cur_date + timedelta(days=1)

    def get_item(self):
        raw_dur = self.__inactive_days[self.__cur_item]
        self.__cur_item = (self.__cur_item + 1) % len(self.__inactive_days)
        self.__pk += 1
        duration = {
            "model": "entrytodoc.InactiveDays",
            "pk": self.__pk,
            "fields": {
                "start_date": raw_dur.beg.strftime(self.DATE_FORMAT),
                "end_date": raw_dur.end.strftime(self.DATE_FORMAT),
                "doctors_regex": raw_dur.doc_regex,
                "description": raw_dur.descr
            }
        }
        return duration

    def get_items(self, count=0):
        days = []
        for item in range(len(self.__inactive_days)):
            days.append(self.get_item())
        return days

if __name__ == "__main__":
    output_dir = ""#"output/"
    input_dir = "input/"
#    try:
    with open(output_dir + "inactivedays.json", "w", encoding="utf-8") as idout:
        inactive_days_gen = InactiveDaysGenerator()
        inactive_days = inactive_days_gen.get_items()
        json.dump(inactive_days, idout, indent=4, ensure_ascii=False)
    sys.exit(0)
    with open(output_dir + "polyclinic.json", "w", encoding="utf-8") as pout, \
            open(input_dir+"regions_cities.json", encoding="utf-8") as r_s_in, \
            open(input_dir+"streets.json", encoding="utf-8") as st_in:
        polyclinic_gen = PolyclinicGenerator(
            json.load(r_s_in), json.load(st_in)
        )
        count = 20
        polyclinics = polyclinic_gen.get_items(count)
        json.dump(polyclinics, pout, indent=4, ensure_ascii=False)
#    except Exception as error:
#        print(str(error))
#    try:
    doctors = {}
    docs_per_clinic = 30
    with open(output_dir+"user.json", "w", encoding="utf-8") as uout, \
            open(output_dir + "patient.json", "w", encoding="utf-8") as pout, \
            open(output_dir + "doctor.json", "w", encoding="utf-8") as dout, \
            open(input_dir + "first_names.json", encoding="utf-8") as f_n_in, \
            open(input_dir + "second_names.json", encoding="utf-8") as s_n_in, \
            open(input_dir + "patronymics.json", encoding="utf-8") as p_in, \
            open(input_dir + "specialities.json", encoding="utf-8") as spec_in:
        count = 400 * len(polyclinics)
        user_gen = UserGenerator()
        users = user_gen.get_items(count)
        json.dump(users, uout, indent=4, ensure_ascii=False)
        fnames_data = json.load(f_n_in)
        snames_data = json.load(s_n_in)
        patr_data = json.load(p_in)
        patient_gen = PatientGenerator(
            fnames_data, snames_data, patr_data
        )
        patients = patient_gen.get_items(len(users))
        json.dump(patients, pout, indent=4, ensure_ascii=False)
        doctor_gen = DoctorGenerator(
            fnames_data, snames_data, patr_data, json.load(spec_in)
        )
        count = docs_per_clinic*len(polyclinics)
        doctors = doctor_gen.get_items(count)
        json.dump(doctors, dout, indent=4, ensure_ascii=False)
#    except Exception as error:
#        print(str(error))



#    try:
        work_days = 5
        with open(output_dir + "schedule.json", "w", encoding="utf-8") as sout:
            schedule_gen = ScheduleGenerator(len(doctors), len(polyclinics), docs_per_clinic)
            count = work_days * len(doctors)
            schedule = schedule_gen.get_items(count)
            json.dump(schedule, sout, indent=4, ensure_ascii=False)
#    except Exception as error:
#        print(str(error))

#    try:
        with open(output_dir + "letter.json", "w") as lout:
            letter_gen = LetterGenerator()
            count = 10
            letters = letter_gen.get_items(count)
            json.dump(letters, lout, indent=4)
#    except Exception as error:
#        print(str(error))


