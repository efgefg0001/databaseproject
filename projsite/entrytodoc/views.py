#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from django.shortcuts import *
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils import translation
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django.contrib.auth import logout
from django.core import serializers
import jsonpickle

from entrytodoc.forms import *
from .states import *


class RegisterView(View):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        registered = False
        user_form = UserForm()
        patient_form = PatientForm()
        # Render the template depending on the context.
        return render_to_response("entrytodoc/register.html",
            {"user_form": user_form, "patient_form": patient_form, "registered": registered}, context)

    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        registered = False
        user_form = UserForm(data=request.POST)
        patient_form = PatientForm(data=request.POST)
        if user_form.is_valid() and patient_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            patient = patient_form.save(commit=False)
            patient.user = user
            patient.save()
            registered = True
        else:
            print(user_form.errors, patient_form.errors)
        return render_to_response("entrytodoc/register.html",
            {"user_form": user_form, "patient_form": patient_form, "registered": registered}, context)


class UserLoginView(View):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        return render_to_response("entrytodoc/login.html", {}, context)
    def __return_type_and_id_of_user(self, username):
        user_type = "none"
        id = 0
        patient_id = queries.get_patient_id(username)
        doctor_id = queries.get_doctor_id(username)
        if patient_id != 0:
            user_type = "patient"
            id = patient_id
        elif doctor_id != 0:
            user_type = "doctor"
            id = doctor_id
        return user_type, id

    def __set_user_id_and_type_in_session(self, username, session):
        user_type, id = self.__return_type_and_id_of_user(username)
        if user_type == "doctor":
            session["user_type"] = user_type
            session["doctor_id"] = id
        elif user_type == "patient":
            session["user_type"] = user_type
            session["patient_id"] = id

    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        username = request.POST["username"].strip()
        password = request.POST["password"].strip()
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                self.__set_user_id_and_type_in_session(username, request.session)
                return HttpResponseRedirect("/entrytodoc/")
            else:
                return HttpResponse("Your Clinic account is disabled.")
        else:
            print("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")


# Use the login_required() decorator to ensure only those logged in can access the view.
#@login_required
def user_logout(request):
    print("logout\n")
    # Since we know the user is logged in, we can now just log them out.
    logout(request)
    print("logout")
    # Take the user back to the homepage.
    return HttpResponseRedirect("/entrytodoc/")


class ProtectedView(TemplateView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class CabinetView(ProtectedView):
    def __create_letter_buf(self, patient_id):
        letter_buf = LetterBuffer()
        letter_buf.patient = patient_id
        return jsonpickle.encode(letter_buf)

    def get(self, request, *args, **kwargs):
        context_dict = {"user_type": request.session["user_type"]}
        if context_dict["user_type"] == "patient":
            request.session["cur_state"] = 0
            request.session["letter_buf"] = self.__create_letter_buf(request.session["patient_id"])
        context = RequestContext(request)
        return render_to_response("entrytodoc/cabinet.html", context_dict, context)

class OrderLetterView(ProtectedView):

    __states = [
        P(), D(), SCH(), L(), E()
    ]
    __clinic_id = 0
    __var_dict = defaultdict()
    __serializer = serializers.get_serializer("json")()

    def __next_state(self, cur_state):
        return (cur_state + 1) % len(self.__states)

    def __prev_state(self, cur_state):
        return (cur_state - 1) % len(self.__states)

    def __restore_from_session(self, session):
#        form = context_dict["order_letter_form"]
#        def_val = jsonpickle.encode(form.fields["doctors"].choices)
#        form.fields["doctors"].choices = \
#            jsonpickle.decode(session.get("doctors_choices", def_val))

#        def_val = jsonpickle.encode(Calendar(weekdays=["Mo", "Te"], dates=[1, 2]))
#        data = session.get("schedule", def_val)
#        print(data)
#        obj = jsonpickle.decode(data)
#        context_dict["schedule"] = \
#            jsonpickle.decode(session.get("schedule", def_val))
#        print("restore")
#        if "letter_buf" not in session.keys():
#            let_buf = LetterBuffer()
#            let_buf.patient = session["patient_id"]
#            def_val = jsonpickle.encode(let_buf)
#            session["letter_buf"] = def_val
#            letter_buf = let_buf
#        else:
#        print(session["letter_buf"])
        return jsonpickle.decode(session["letter_buf"])
#        print("restore pat id = {}".format(letter_buf.patient))


    def __set_session_fields(self, session, letter_buf):
#        form = context_dict["order_letter_form"]
#        session["doctors_choices"] = jsonpickle.encode(form.fields["doctors"].choices)
#        print(context_dict["schedule"])
#        session["schedule"] = jsonpickle.encode(context_dict["schedule"])
        session["letter_buf"] = jsonpickle.encode(letter_buf)

    def __create_context_dict(self, form, session):
        context_dict = defaultdict()
        context_dict["order_letter_form"] = form
        self.__set_context_dict(context_dict, session)
        return context_dict

    def get(self, request, *args, **kwargs):
        print("OrderLetter get")
        context = RequestContext(request)
        order_letter_form = OrderLetterForm({})
        context_dict = {"order_letter_form": order_letter_form}
        cur_state = request.session.get("cur_state")
        if cur_state is None:
            cur_state = 0
            request.session["cur_state"] = cur_state
        if cur_state == len(self.__states) -1:
            request.session["cur_state"] = 0
        letter_buf = self.__restore_from_session(request.session)
        self.__states[cur_state].run(request.GET, context_dict, letter_buf, "GET")
        return render_to_response("entrytodoc/order_letter.html", context_dict, context)

    def post(self, request, *args, **kwargs):
        print("OrderLetter post")
        context = RequestContext(request)
        order_letter_form = OrderLetterForm(data=request.POST)
        context_dict = {"order_letter_form": order_letter_form}
        letter_buf = self.__restore_from_session(request.session)
        #self.__create_context_dict(order_letter_form, request.session)
        cur_state = request.session.get("cur_state")
        if cur_state is None:
            cur_state = 0
            request.session["cur_state"] = cur_state
        if "back" in request.POST.dict().keys() and request.POST["back"] == "prev_state":
            cur_state = self.__prev_state(cur_state)
            request.session["cur_state"] = cur_state
            self.__states[cur_state].run(request.POST, context_dict, letter_buf, "GET")
        else:
            self.__states[cur_state].prepare(letter_buf, context_dict)
            if order_letter_form.is_valid():
                ch_state = self.__states[cur_state].run(request.POST, context_dict, letter_buf, "POST")
                if ch_state:
                    cur_state = self.__next_state(cur_state)
                    request.session["cur_state"] = cur_state
                    self.__set_session_fields(request.session, letter_buf)
            else:
                print(order_letter_form.errors)
        context_dict["state"] = self.__states[cur_state]
        return HttpResponseRedirect("/entrytodoc/cabinet/order_letter")
#            render_to_response("entrytodoc/order_letter.html", context_dict, context)


class ShowYourLettersView(ProtectedView):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        active_letters = queries.get_your_active_letters(request.session["patient_id"])
        is_empty = False
        if len(active_letters) == 0:
            is_empty = True
        context_dict = {
            "active_letters": active_letters,
            "is_empty": is_empty,
        }
        return render_to_response("entrytodoc/show_your_letters.html", context_dict, context)

    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        letter_id = request.POST["cancel_letter"]
        try:
            letter = Letter.objects.get(pk=letter_id)
            if letter is not None:
                letter.patient = None
                letter.save()
        except Exception as error:
            print(str(error))
        return HttpResponseRedirect("/entrytodoc/cabinet/show_your_letters/")


class ShowMyPatientsView(ProtectedView):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
#        print("doctor_id = {}".format(request.session["doctor_id"]))
        patient_letters = queries.get_my_patients(request.session["doctor_id"])
        is_empty = False
        if len(patient_letters) == 0:
            is_empty = True
        context_dict = {
            "patient_letters": patient_letters,
            "is_empty": is_empty,
        }
        return render_to_response("entrytodoc/show_my_patients.html", context_dict, context)

    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        letter_id = request.POST["take_patient"]
        try:
            letter = Letter.objects.get(pk=letter_id)
            if letter is not None:
                letter.patient = None
                log = letter.log
                print(letter.log)
                if log is not None:
                    print(log)
                    log.doc_took = True;
                    log.save()
                    letter.log = None;
                letter.save()
        except Exception as error:
            print(str(error))
        return HttpResponseRedirect("/entrytodoc/cabinet/show_my_patients/")


class ChangePersonalDataView(ProtectedView):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        patient_form = None
        if request.session["user_type"] == "patient":
            patient_id = request.session["patient_id"]
            patient = Patient.objects.get(pk=patient_id)
            patient_form = PatientForm(instance=patient)
        return render_to_response("entrytodoc/change_personal_data.html", \
                                  {"patient_form": patient_form, "user_type": "patient"}, context)

    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        success = False
        if request.session["user_type"] == "patient":
            patient_form = PatientForm(data=request.POST, instance=Patient.objects.get(pk=request.session["patient_id"]))
            if patient_form.is_valid():
                patient_form.save()
                success = True
            else:
                print(patient_form.errors)
        return render_to_response("entrytodoc/change_personal_data.html", \
                                  {"patient_form": patient_form, "success": success, "user_type": "patient"}, context)
        #HttpResponseRedirect("/entrytodoc/cabinet/")


class ShowMyPersonalData(ProtectedView):

    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        doctor_form = DoctorForm(instance=Doctor.objects.get(pk=request.session["doctor_id"]))
        return render_to_response("entrytodoc/show_my_personal_data.html", {"doctor_form": doctor_form}, context)





