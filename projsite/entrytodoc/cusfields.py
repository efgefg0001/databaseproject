#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from django.utils.translation import ugettext as _
from django.db.models import CharField

DAY_OF_THE_WEEK = {
    "0" : _("Monday"),
    "1" : _("Tuesday"),
    "2" : _("Wednesday"),
    "3" : _("Thursday"),
    "4" : _("Friday"),
    "5" : _("Saturday"),
    "6" : _("Sunday"),
}


class DayOfTheWeekField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs["choices"]=tuple(sorted(DAY_OF_THE_WEEK.items()))
        kwargs["max_length"]=1
        super().__init__(*args, **kwargs)
