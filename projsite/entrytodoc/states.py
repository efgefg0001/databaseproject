#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import abc
from collections import defaultdict, namedtuple
from datetime import date, datetime, timedelta
import re

from django import forms
from django.utils import timezone
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from entrytodoc.models import *
from entrytodoc.forms import *
from entrytodoc import  queries


class State(metaclass=abc.ABCMeta):

    def __init__(self):
        self.__name = self.__class__.__name__

    def prepare(self, *data):
        pass

    def name(self):
        return self.__name

    def run(self, *data):
        print(self.name())


class P(State):

    def run(self, *data):
        ch_state = False
        req_data, context_dict, letter_buf, mode = data[0], data[1], data[2], data[3]
        order_letter_form = context_dict["order_letter_form"]
        if mode == "POST":
            try:
                polyclinic = order_letter_form.data["polyclinics"]
                if polyclinic != 0:
                    letter_buf.polyclinic = polyclinic
                    order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
                    ch_state = True
            except Exception as error:
                print(str(error))
        else:
            context_dict["state"] = self
        return ch_state

Day = namedtuple("Day", "id date is_inactive")
Calendar = namedtuple("Calendar", "weekdays dates")

DURATION = 14

class D(State):

    def prepare(self, *data):
        letter_buf, context_dict = data
        order_letter_form = context_dict["order_letter_form"]
        order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)

    def run(self, *data):
        ch_state = False
        req_data, context_dict, letter_buf, mode = data[0], data[1], data[2], data[3]
        order_letter_form = context_dict["order_letter_form"]
        schedule = []
        if mode == "POST":
            try:
                doc = req_data["doctors"]
                print("doc = {}".format(doc))
                if doc != 0:
                    print("\nSCH, doctor={}\n".format(Doctor.objects.get(pk=doc)))
                    letter_buf.doctor = doc
                    schedule = queries.get_schedule(letter_buf.doctor, DURATION)
                    ch_state = True
                else:
                    order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
            except Exception as error:
#                order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
                print(str(error))
            finally:
                context_dict["order_letter_form"] = order_letter_form
                context_dict["schedule"] = schedule
        else:
            order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
            context_dict["state"] = self
        return ch_state


class SCH(State):

    def prepare(self, *data):
        letter_buf, context_dict = data
        order_letter_form = context_dict["order_letter_form"]
        order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
        context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)

    def run(self, *data):
        ch_state = False
        req_data, context_dict, letter_buf, mode = data[0], data[1], data[2], data[3]
        order_letter_form = context_dict["order_letter_form"]
        letter_times = [[]]
        if mode == "POST":
            try:
#                assert False, "SCH"
                letter_buf.date = datetime.strptime(req_data["get_date"], "%Y-%m-%d").date()
                letter_times = queries.get_letter_times(letter_buf)
                ch_state = True
            except Exception as error:
                print(str(error))
#                context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)
            finally:
                context_dict["order_letter_form"] = order_letter_form
                context_dict["letter_times"] = letter_times
        else:
            context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)
            context_dict["state"] = self
        return ch_state


class L(State):
    def prepare(self, *data):
        letter_buf, context_dict = data
        order_letter_form = context_dict["order_letter_form"]
        order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
        context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)
        context_dict["letter_times"] = queries.get_letter_times(letter_buf)

    def run(self, *data):
        ch_state = False
        req_data, context_dict, letter_buf, mode = data[0], data[1], data[2], data[3]
        order_letter_form = context_dict["order_letter_form"]
        if mode == "POST":
            try:
                letter_id = req_data["get_letter_id"]
                letter = Letter.objects.get(pk=letter_id)
                letter.patient = Patient.objects.get(pk=letter_buf.patient)
                log = Log(
                    pat_second_name = letter.patient.second_name,
                    pat_first_name = letter.patient.first_name,
                    pat_patronymic = letter.patient.patronymic,
                    pat_gender = letter.patient.gender,
                    pat_series_of_passport = letter.patient.series_of_passport,
                    pat_passport_number = letter.patient.passport_number,
                    pat_assurance_number = letter.patient.assurance_number,
                    pat_date_of_birth = letter.patient.date_of_birth,
                    pat_phone_number = letter.patient.phone_number,
                    doc_second_name = letter.doctor.second_name,
                    doc_first_name = letter.doctor.first_name,
                    doc_patronymic = letter.doctor.patronymic,
                    doc_speciality = letter.doctor.speciality,
                    doc_phone_number = letter.doctor.phone_number,
                    letter_date = letter.date,
                    letter_time = letter.time,
                    doc_took = False
                )
                log.save()
                letter.log = log
                letter.save()
#                print("letter_id = {}".format(letter_id))
#                assert False, "SCH"
                ch_state = True
            except Exception as error:
                print(str(error))
#                context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)
            finally:
                pass
        else:
            context_dict["letter_times"] = queries.get_letter_times(letter_buf)
            context_dict["state"] = self
        return ch_state

class E(State):
    def prepare(self, *data):
        letter_buf, context_dict = data
        order_letter_form = context_dict["order_letter_form"]
        order_letter_form.fields["doctors"].choices = queries.get_doctors(letter_buf.polyclinic)
        context_dict["schedule"] = queries.get_schedule(letter_buf.doctor, DURATION)
        context_dict["letter_times"] = queries.get_letter_times(letter_buf)

    def run(self, *data):
        ch_state = False
        req_data, context_dict, letter_buf, mode = data[0], data[1], data[2], data[3]
        context_dict["state"] = self
        return ch_state

class LetterBuffer:

    def __init__(self):
        self.patient = 0
        self.polyclinic = 0
        self.doctor = 0
        self.date = date(1, 1, 1)

    @property
    def patient(self):
        return self.__patient

    @patient.setter
    def patient(self, value):
        self.__patient = value
    
    @property
    def polyclinic(self):
        return self.__polyclinic
    
    @polyclinic.setter
    def polyclinic(self, value):
        self.__polyclinic = value
    
    @property
    def doctor(self):
        return self.__doctor
    
    @doctor.setter
    def doctor(self, value):
        self.__doctor = value
    
    @property
    def time(self):
        return self.__time
    
    @time.setter
    def time(self, value):
        self.__time = value

    @property
    def date(self):
        return self.__date

    @date.setter
    def date(self, value):
        self.__date = value
