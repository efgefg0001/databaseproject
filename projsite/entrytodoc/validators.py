#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def validate_passport_series(value):
    """
    Проверить валидность серии паспорта.
    """
    pattern = r"^\d{4}$"
    if re.match(pattern, value) is None:
        raise ValidationError(
#            _(" is not a passport series")
            "{} не является серией паспорта".format(value)
        )


def validate_passport_number(value):
    """
    Проверить валидность номера паспорта.
    """
    pattern = r"^\d{6}$"
    if re.match(pattern, value) is None:
        raise ValidationError(
#            _(" is not a passport number")
            "{} не является номером паспорта".format(value)
        )


def validate_assurance_number(value):
    """
    Проверить валидность номера
    полиса медицинского страхования.
    """
    pattern = r"^\d{16}$"
    if re.match(pattern, value) is None:
        raise ValidationError(
#            _(" is not an assurance number")
            "{} не является номером полиса".format(value)
        )


def validate_phone_number(value):
    """
    Проверить валидность номера телефона.
    """
    pattern = r"^\d{11}$"
    if re.match(pattern, value) is None:
        raise ValidationError(
#            _(" is not an assurance number")
            "{} не является номером телефона".format(value)
        )
