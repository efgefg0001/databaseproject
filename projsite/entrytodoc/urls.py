from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView, RedirectView
from entrytodoc.views import *

urlpatterns = patterns("",
    # Examples:
    # url(r"^$", "projsite.views.home", name="home"),
    # url(r"^blog/", include("blog.urls")),
    url(r"^$", TemplateView.as_view(template_name="entrytodoc/index.html"), name="index"),
    url(r"^register/$", RegisterView.as_view(), name="register"),
    url(r"^login/$", UserLoginView.as_view(), name="login"),
    url(r"^logout/$", user_logout, name="logout"),
    url(r"^cabinet/$", CabinetView.as_view(), name="cabinet"),
    url(r"^cabinet/order_letter/$", OrderLetterView.as_view(),
        name="order_letter"),
    url(r"^cabinet/change_personal_data/$", ChangePersonalDataView.as_view(),
        name="change_personal_data"),
    url(r"^cabinet/show_my_personal_data/$", ShowMyPersonalData.as_view(),
        name="show_my_personal_data"),
    url(r"^cabinet/show_your_letters/$", ShowYourLettersView.as_view(),
        name="show_your_letters"),
    url(r"^cabinet/show_my_patients/$", ShowMyPatientsView.as_view(),
        name="show_my_patients"),

)