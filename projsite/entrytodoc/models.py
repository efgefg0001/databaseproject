#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import  ugettext_lazy as _
from datetime import datetime
from .cusfields import *


class Patient(models.Model):
    """
    Модель для таблицы пациентов.
    """
    patient_id = models.AutoField(_("patient id"), primary_key=True)
    user = models.OneToOneField(User)
    second_name = models.CharField(_("second name"), max_length=100)
    first_name = models.CharField(_("first name"), max_length=100)
    patronymic = models.CharField(_("patronymic"), max_length=100)
    gender = models.CharField(_("gender"), max_length=1)
    series_of_passport = models.CharField(_("series of passport"), max_length=4)
    passport_number = models.CharField(_("passport number"), max_length=6)
    assurance_number = models.CharField(_("assurance number"), max_length=16)
    date_of_birth = models.DateField(_("date of birth"))
    phone_number = models.CharField(_("phone number"), max_length=11)


class Doctor(models.Model):
    """
    Модель для таблицы докторов.
    """
    doctor_id = models.AutoField(_("doctor id"), primary_key=True)
    user = models.OneToOneField(User, null=True, blank=True)
    second_name = models.CharField(_("second name"), max_length=100)
    first_name = models.CharField(_("first name"), max_length=100)
    patronymic = models.CharField(_("patronymic"), max_length=100)
    speciality = models.CharField(_("speciality"), max_length=100)
    phone_number = models.CharField(_("phone number"), max_length=100)

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}".format(self.second_name, self.first_name, self.patronymic, self.speciality, self.phone_number)


class Log(models.Model):
    """
    Модель для таблицы лога.
    """
    log_id = models.AutoField(_("log id"), primary_key=True)
    pat_second_name = models.CharField(_("second name"), max_length=100)
    pat_first_name = models.CharField(_("first name"), max_length=100)
    pat_patronymic = models.CharField(_("patronymic"), max_length=100)
    pat_gender = models.CharField(_("gender"), max_length=1)
    pat_series_of_passport = models.CharField(_("series of passport"), max_length=4)
    pat_passport_number = models.CharField(_("passport number"), max_length=6)
    pat_assurance_number = models.CharField(_("assurance number"), max_length=16)
    pat_date_of_birth = models.DateField(_("date of birth"))
    pat_phone_number = models.CharField(_("phone number"), max_length=11)
    doc_second_name = models.CharField(_("second name"), max_length=100)
    doc_first_name = models.CharField(_("first name"), max_length=100)
    doc_patronymic = models.CharField(_("patronymic"), max_length=100)
    doc_speciality = models.CharField(_("speciality"), max_length=100)
    doc_phone_number = models.CharField(_("phone number"), max_length=100)
    letter_date = models.DateField()
    letter_time = models.TimeField()
    doc_took = models.BooleanField(default=False)


class Letter(models.Model):
    """
    Модель для таблицы талонов.
    """
    letter_id = models.AutoField(_("letter id"), primary_key=True)
    patient = models.ForeignKey(Patient, null=True, blank=True)
    doctor = models.ForeignKey(Doctor)
    time = models.TimeField(_("time"))
    date = models.DateField(_("date"))
    log = models.ForeignKey(Log, null=True, blank=True)


class Polyclinic(models.Model):
    """
    Модель для таблицы поликлиник.
    """
    polyclinic_id = models.AutoField(_("polyclinic id"), primary_key=True)
    region = models.CharField(_("region"), max_length=100)
    location = models.CharField(_("location"), max_length=100)
    street = models.CharField(_("street"), max_length=100)
    building = models.CharField(_("building"), max_length=100)
    polyclinic_name = models.CharField(_("polyclinic name"), max_length=100)

    def __str__(self):
        return "{0}, {1}, {2}, {3}, {4}".format(self.polyclinic_name, self.region, self.location, self.street, self.building)


class Schedule(models.Model):
    """
    Модель для таблицы расписания.
    """
    schedule_row_id = models.AutoField(_("schedule row id"), primary_key=True)
    doctor = models.ForeignKey(Doctor)
    day_of_week = DayOfTheWeekField(_("day of week"))
    start_time = models.TimeField(_("start time"))
    end_time = models.TimeField(_("end time"))
    polyclinic = models.ForeignKey(Polyclinic)
    cabinet_number = models.IntegerField(_("cabinet number"))

    def __str__(self):
        return _("Weekday: {0}, start time: {1}, end time: {2}, cabinet number: {3}").format(
            self.day_of_week, self.start_time, self.end_time, self.cabinet_number)


class InactiveDays(models.Model):
    """
    Модель для таблицы нерабочих дней.
    """
    inactive_days_id = models.AutoField(_("inactive day id"), primary_key=True)
    start_date = models.DateField(_("start date"))
    end_date = models.DateField(_("end date"))
    doctors_regex = models.CharField(_("doctors regex"), max_length=100)
    description = models.CharField(_("description"), max_length=100)
