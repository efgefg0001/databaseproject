#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from entrytodoc.models import *
from django.contrib.auth.models import User
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.admin.widgets import *

from entrytodoc.validators import *


class UserForm(forms.ModelForm):
    username = forms.CharField(label=_("Username"))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "password")


class PatientForm(forms.ModelForm):
    date_of_birth = forms.DateField(input_formats=["%d.%m.%Y", "%d-%m%-%Y", "%Y.%m.%d", "%Y-%m-%d"], label=_("Date of birth"))
    gender = forms.ChoiceField(label=_("Gender"),
        choices=[
            ("М", _("M")),
            ("Ж", _("F"))
    ])
    series_of_passport = forms.CharField(validators=[validate_passport_series], max_length=4, label=_("Passport series"))
    passport_number = forms.CharField(validators=[validate_passport_number], max_length=6, label=_("Passport number"))
    assurance_number = forms.CharField(validators=[validate_assurance_number], max_length=16, label=_("Assurance number"))
    phone_number = forms.CharField(validators=[validate_phone_number], max_length=11, label=_("Phone number"))
    class Meta:
        model = Patient
        fields = (
            "second_name",
            "first_name",
            "patronymic",
            "gender",
            "series_of_passport",
            "passport_number",
            "assurance_number",
            "date_of_birth",
            "phone_number",
        )





BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
FAVORITE_COLORS_CHOICES = (('blue', 'Blue'),
                            ('green', 'Green'),
                            ('black', 'Black'))
class OrderLetterForm(forms.Form):

#    clinic = forms.ModelChoiceField(queryset=Polyclinic.objects.all())
    polyclinics = forms.ModelChoiceField(widget=forms.RadioSelect(), queryset=Polyclinic.objects.all(), empty_label=None,
                                         required=False)
    doctors = forms.ChoiceField(widget=forms.RadioSelect(),choices=[(1, 1)], required=False)
#    schedule = forms.ChoiceField(widget=forms.RadioSelect(),
#                                 choices=[(1, 1)], required=False)
#    def __init__(self, *args, **kwargs):
#        super(OrderLetterForm, self).__init__(*args, **kwargs)
#        self.fields["schedule"] = [1, 2, 3, 4, 5]
#        self.fields["doctors"] = forms.ChoiceField(widget=forms.RadioSelect(),
#                                                   choices= doctors_choices,
#                                                   required=False)

class CalendarForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["date"] = forms.DateField(widget=AdminDateWidget(), initial=datetime.today())

#    class Meta:
#        {
#            "date": AdminDateWidget(),
#        }


class DoctorForm(forms.ModelForm):
    class Meta:
       model = Doctor
       fields = (
           "second_name",
           "first_name",
            "patronymic",
            "speciality",
            "phone_number"
       )
       widgets = {
           "second_name": forms.TextInput(attrs={"class": "disabled", "readonly": "readonly"}),
           "first_name": forms.TextInput(attrs={"class": "disabled", "readonly": "readonly"}),
           "patronymic": forms.TextInput(attrs={"class": "disabled", "readonly": "readonly"}),
           "speciality": forms.TextInput(attrs={"class": "disabled", "readonly": "readonly"}),
           "phone_number": forms.TextInput(attrs={"class": "disabled", "readonly": "readonly"}),
       }
