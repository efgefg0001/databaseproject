#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from entrytodoc.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projsite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^entrytodoc/', include('entrytodoc.urls', namespace="entrytodoc")),
    url(r'^admin/', include(admin.site.urls)),
)
